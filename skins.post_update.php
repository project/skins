<?php

/**
 * @file
 * Post update functions for Skins.
 */

/**
 * Clear caches due to changes in service arguments.
 */
function skins_post_update_event_dispatch() {
  // Empty post-update hook.
}

/**
 * Clear caches due to changes in service arguments.
 */
function skins_post_update_condition() {
  // Empty post-update hook.
}

/**
 * Clear caches due to new service.
 */
function skins_post_update_template_suggester() {
  // Empty post-update hook.
}
