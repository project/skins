<?php

use Drupal\skins\Form\ThemeSettings;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_system_info_alter().
 *
 * Merges in data from themes' active skins.
 */
function skins_system_info_alter(array &$info, Extension $file, $type) {
  $skin_handler = \Drupal::service('skins_handler');
  if ($type === 'theme') {
    $theme_name = $file->getName();
    $active_skin = \Drupal::config($theme_name . '.settings')->get('skin');
    if ($active_skin && $skins = $skin_handler->getThemeSkins($theme_name)) {
      // Don't merge in name or description. That would be confusing. Plus
      // we've already translated them.
      // No sense in overriding the screenshot either.
      unset($skins[$active_skin]['name']);
      unset($skins[$active_skin]['description']);
      unset($skins[$active_skin]['screenshot']);
      $info = NestedArray::mergeDeep($info, $skins[$active_skin]);
    }
  }
}

/**
 * Implements hook_theme_suggestions_alter().
 *
 * If the active theme has a selected skin, add suggestions for that skin.
 */
function skins_theme_suggestions_alter(array &$suggestions, array $variables, $hook) {
  \Drupal::service('skins_template_suggester')->addSkinSuggestions($suggestions, $hook);
}

/**
 * Implements hook_module_implements_alter().
 */
function skins_module_implements_alter(&$implementations, $hook) {
  if ($hook === 'theme_suggestions_alter') {
    // Move skins_theme_suggestions_alter() to the end of the list.
    $group = $implementations['skins'];
    unset($implementations['skins']);
    $implementations['skins'] = $group;
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for system_theme_settings.
 */
function skins_form_system_theme_settings_alter(&$form, FormStateInterface $form_state, $form_id = NULL) {
  $skin_handler = \Drupal::service('skins_handler');
  $build_info = $form_state->getBuildInfo();
  $theme_name = !empty($build_info['args'][0]) ? $build_info['args'][0] : FALSE;
  if ($theme_name && $skins = $skin_handler->getThemeSkins($theme_name)) {
    // Get the theme's default screenshot.
    $info = \Drupal::service('extension.list.theme')
      ->getExtensionInfo($theme_name);
    if (isset($info['screenshot'])) {
      $theme_screenshot_uri = $info['screenshot'];
    }
    else {
      $theme_screenshot_uri = \Drupal::service('extension.list.module')->getPath('system') . '/images/no_screenshot.png';
    }

    $form['#submit'][] = [ThemeSettings::class, 'submit'];

    $form['skins'] = [
      '#type' => 'details',
      '#title' => t('Skins'),
      '#description' => t('Select a skin to use for this theme.'),
      '#weight' => -5,
      '#open' => TRUE,
      '#attributes' => [
        'class' => ['system-themes-list-installed', 'clearfix'],
      ],
    ];
    $form['skins']['skin'] = [
      '#type' => 'radios',
      '#title' => t('Skin'),
      '#options' => [
        '' => t('None')
      ],
      '#default_value' => \Drupal::config($theme_name . '.settings')->get('skin') ?: '',
      '#skins' => $skins,
      '#theme_screenshot_uri' => $theme_screenshot_uri,
      '#pre_render' => [[ThemeSettings::class, 'preRenderSkinRadios']],
    ];
    // For each of the available skins, add a preview image.
    foreach ($skins as $skin_id => $skin) {
      $form['skins']['skin']['#options'][$skin_id] = Html::escape($skin['name']);
    }
  }
}
