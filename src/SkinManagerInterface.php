<?php

namespace Drupal\skins;

/**
 * Defines an interface to list available skins.
 */
interface SkinManagerInterface {

  /**
   * Returns a key indicating the active theme and skin.
   *
   * @return string
   *   A key in the form theme_name:skin_name.
   */
  public function getActiveThemeAndSkinKey();

}
