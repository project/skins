<?php

namespace Drupal\skins\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\skins\SkinManagerInterface;

/**
 * Defines the SkinCacheContext service, for "per theme, per skin" caching.
 *
 * Cache context ID: 'skin'.
 */
class SkinCacheContext implements CacheContextInterface {

  /**
   * The theme manager.
   *
   * @var \Drupal\skins\SkinManagerInterface
   */
  protected $skinManager;

  /**
   * Constructs a new SkinCacheContext service.
   *
   * @param \Drupal\skins\SkinManagerInterface $skin_manager
   *   The theme manager.
   */
  public function __construct(SkinManagerInterface $skin_manager) {
    $this->skinManager = $skin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Skin');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return $this->skinManager->getActiveThemeAndSkinKey();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
