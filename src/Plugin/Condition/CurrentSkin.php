<?php

namespace Drupal\skins\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\skins\SkinHandlerInterface;
use Drupal\skins\SkinManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Current Skin' condition.
 *
 * @Condition(
 *   id = "current_skin",
 *   label = @Translation("Current Skin"),
 * )
 */
class CurrentSkin extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The skin manager.
   *
   * @var \Drupal\skins\SkinManagerInterface
   */
  protected $skinManager;

  /**
   * The skin handler.
   *
   * @var \Drupal\skins\SkinHandlerInterface
   */
  protected $skinHandler;

  /**
   * Constructs a CurrentThemeCondition condition plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   * @param \Drupal\skins\SkinManagerInterface $skin_manager
   *   The skin manager.
   * @param \Drupal\skins\SkinHandlerInterface $skin_handler
   *   The skin handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ThemeHandlerInterface $theme_handler, SkinManagerInterface $skin_manager, SkinHandlerInterface $skin_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->themeHandler = $theme_handler;
    $this->skinManager = $skin_manager;
    $this->skinHandler = $skin_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('theme_handler'),
      $container->get('skins_manager'),
      $container->get('skins_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['skin' => ''] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $options = [
      '' => $this->t('- Not specified -')
    ];

    $themes = array_map(function ($theme_info) {
      return $theme_info->info['name'];
    }, $this->themeHandler->listInfo());
    foreach ($themes as $theme_name => $theme_title) {
      if ($this->skinHandler->themeHasSkins($theme_name)) {
        $skins = array_map(function ($skin) {
          return $skin['name'];
        }, $this->skinHandler->getThemeSkins($theme_name));
        foreach ($skins as $skin_name => $skin_title) {
          $options[$theme_name . ':' . $skin_name] = $this->t('@theme_title - @skin_title', ['@theme_title' => $theme_title, '@skin_title' => $skin_title]);
        }
      }
    }

    $form['skin'] = [
      '#type' => 'radios',
      '#title' => $this->t('Skin'),
      '#default_value' => $this->configuration['skin'],
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', $options),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['skin'] = $form_state->getValue('skin');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration['skin'])) {
      return TRUE;
    }

    return $this->skinManager->getActiveThemeAndSkinKey() == $this->configuration['skin'];
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    if ($this->isNegated()) {
      return $this->t('The current skin is not @skin', ['@skin' => $this->configuration['skin']]);
    }

    return $this->t('The current skin is @skin', ['@skin' => $this->configuration['skin']]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'skin';
    return $contexts;
  }

}
