<?php

namespace Drupal\skins;

/**
 * Defines an interface to suggest skin-specific templates.
 *
 * @internal
 */
interface SkinTemplateSuggesterInterface {

  /**
   * If the active theme has a selected skin, adds suggestions for that skin.
   *
   * This method should be called from skin-enabled themes in
   * hook_theme_suggestions_alter() to add skin template suggestions for
   * suggestions registered by modules in hook_theme_suggestions_HOOK_alter().
   * Sample code:
   * @code
   * function mytheme_theme_suggestions_alter(array &$suggestions, array $variables, $hook) {
   *   // While Skins has already made this call, we repeat it to cover any
   *   // changes made by modules in hook_theme_suggestions_HOOK_alter().
   *   \Drupal::service('skins_template_suggester')->addSkinSuggestions($suggestions, $hook);
   * }
   * @endcode
   *
   * The method should can also be called by any implementations of
   * hook_theme_suggestions_HOOK_alter() in the theme, since these are invoked
   * after hook_theme_suggestions_alter(). For efficiency, only the new
   * suggestions may be passed to the ::addSkinSuggestions() method.
   * Sample code:
   * @code
   * function mytheme_theme_suggestions_form_alter(array &$suggestions, array $variables) {
   *   $new_suggestions = ['form__my_new_suggestion'];
   *   \Drupal::service('skins_template_suggester')->addSkinSuggestions($new_suggestions);
   *   $suggestions = array_merge($suggestions, $new_suggestions);
   * }
   * @endcode
   *
   * @param array &$suggestions
   *   An array of alternate, more specific names for template files or theme
   *   functions.
   * @param string $hook
   *   The base hook name. For example, if '#theme' => 'node__article' is called,
   *   then $hook will be 'node', not 'node__article'. The specific hook called
   *   (in this case 'node__article') is available in
   *   $variables['theme_hook_original'].
   */
   public function addSkinSuggestions(array &$suggestions, $hook = NULL);

}
