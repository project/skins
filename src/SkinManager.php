<?php

namespace Drupal\skins;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Theme\ThemeManagerInterface;

/**
 * Defines the SkinManager service.
 */
class SkinManager implements SkinManagerInterface {

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new SkinManager service.
   *
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ThemeManagerInterface $theme_manager, ConfigFactoryInterface $config_factory) {
    $this->themeManager = $theme_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveThemeAndSkinKey() {
    $theme_name = $this->themeManager->getActiveTheme()->getName() ?: 'stark';
    $skin_name = $this->configFactory->get($theme_name . '.settings')->get('skin') ?: '';
    return $theme_name . ':' . $skin_name;
  }

}
