<?php

namespace Drupal\skins;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\skins\SkinHandlerInterface;

/**
 * Suggests templates for the current theme and skin.
 */
class SkinTemplateSuggester implements SkinTemplateSuggesterInterface {

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The skins handler.
   *
   * @var \Drupal\skins\SkinHandlerInterface
   */
  protected $skinsHandler;

  /**
   * Constructs a new SkinTemplateSuggester service.
   *
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\skins\SkinHandlerInterface $skins_handler
   *   The skins handler.
   */
  public function __construct(ThemeManagerInterface $theme_manager, ConfigFactoryInterface $config_factory, SkinHandlerInterface $skins_handler) {
    $this->themeManager = $theme_manager;
    $this->configFactory = $config_factory;
    $this->skinsHandler = $skins_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function addSkinSuggestions(array &$suggestions, $hook = NULL) {
    $active_theme = $this->themeManager->getActiveTheme()->getName();
    if ($this->skinsHandler->themeHasSkins($active_theme) && $theme_skin = $this->configFactory->get($active_theme . '.settings')->get('skin')) {
      $suffix = '__' . $active_theme . '_' . $theme_skin;

      $new_suggestions = [];

      // Add a low-priority suggestion for the hook.
      // If there's already a suggestion for the hook, we don't need to add one
      // because it will be covered below.
      if (!is_null($hook) && !in_array($hook, $suggestions)) {
        $new_suggestions[] = $hook . $suffix;
      }

      // For each existing suggestion, add a skin-specific suggestion that's
      // next highest.
      foreach ($suggestions as $suggestion) {
        $new_suggestions[] = $suggestion;
        // Skip already-processed suggestions and our own previous additions.
        if (!in_array($suggestion . $suffix, $suggestions) && strpos($suggestion, $suffix) === FALSE) {
          $new_suggestions[] = $suggestion . $suffix;
        }
      }
      $suggestions = $new_suggestions;
    }
  }

}
